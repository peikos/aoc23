//! Toolbox of reusable functions.

use itertools::Itertools;
use std::cmp::{Ordering, Ordering::*};
use std::collections::HashMap;
use std::iter::Iterator;
use std::ops::{Add, Sub};

/// An iterator to maintain accumulating state while mapping a function. This `struct` is created
/// by the [`.map_accum()`](crate::prelude::ExtraIterators::map_accum()) method on [`Iterator`]. See its documentation for more.
pub struct MapAccum<I, Acc, F> {
    accu: Acc,
    iter: I,
    f: F,
}

impl<I, Acc, F> MapAccum<I, Acc, F> {
    fn new(iter: I, accu: Acc, f: F) -> Self {
        Self { iter, accu, f }
    }
}

impl<I, B, Acc, F> Iterator for MapAccum<I, Acc, F>
where
    I: Iterator,
    F: FnMut(&Acc, I::Item) -> Option<(Acc, B)>,
{
    type Item = B;

    fn next(&mut self) -> Option<B> {
        let (new_acc, res) = (self.f)(&self.accu, self.iter.next()?)?;
        self.accu = new_acc;
        Some(res)
    }
}

/// An [`Iterator`] blanket implementation that provides extra adaptors and methods, inspired by
/// [`itertools`].
pub trait ExtraIterators: Iterator {
    /// An method intended for use on a character-based [`Iterator`] such as [`.chars()`](std::String.chars()) Given a pair of delimiters (provided as `&str`), consume and return all characters between `delim[0]` and matching `delim[1]`. Consumes but does not return the delimiters. This iterator requires the pair to be balanced, so any additional instances of `delim[0]` encountered need to be closed before the method returns.
    /// ```
    /// # use aoc::prelude::ExtraIterators;
    /// let mut iterator = "[test[string]foo]bar".chars();
    ///
    /// assert_eq!(iterator.take_within("[]"), "test[string]foo");
    /// assert_eq!(iterator.collect::<String>(), "bar");
    /// ```
    fn take_within(&mut self, delim: &str) -> String
    where
        Self: Sized + Iterator<Item = char>,
    {
        let mut nesting = 0;

        // Match opening delimiter, or panic.
        assert_eq!(
            self.next(),
            Some(delim.chars().next().expect("Missing delimiters!")),
            "Expected opening delimiter did not match input."
        );

        self.by_ref()
            .take_while(|&c| {
                nesting += delim
                    .chars()
                    .position(|delim| c == delim)
                    .map_or(0, |idx| [1, -1][idx]);
                nesting >= 0
            })
            .collect::<String>()
    }

    /// A method that checks whether the `Iterator` represents a singular set. If the `Iterator`
    /// contains $ n \geq 0 $ copies of the same item, returns an `Option` containing it; otherwise,
    /// returns `None`.
    ///
    /// ```
    /// # use aoc::prelude::ExtraIterators;
    ///
    /// let vec = vec![1, 1, 1, 1];
    /// assert_eq!(vec.into_iter().singular(), Some(1));
    /// let vec = vec![1, 2, 1, 1];
    /// assert_eq!(vec.into_iter().singular(), None);
    /// let vec = Vec::<()>::new();
    /// assert_eq!(vec.into_iter().singular(), None);
    fn singular(&mut self) -> Option<Self::Item>
    where
        Self::Item: Eq,
    {
        let guess = self.next();

        self.fold(guess, |u, t| if u == Some(t) { u } else { None })
    }

    /// An iterator adapter that takes an [`Iterator`] and maps over it whilst maintaing a mutable
    /// accumulator.
    ///
    /// ```
    /// # use aoc::prelude::ExtraIterators;
    ///
    /// let vec = vec![1, 2, 3, 2, 4];
    ///
    /// let res = vec.iter().map_accum(0, |state:  &i32, current: &i32|
    ///     if current > state {
    ///         Some((*current, true))
    ///     } else {
    ///         Some((*state, false))
    ///     }).collect::<Vec<_>>();
    ///
    /// assert_eq!(res, vec![true, true, true, false, true]);
    /// ```
    fn map_accum<Acc, F, B>(self, accu: Acc, f: F) -> MapAccum<Self, Acc, F>
    where
        Self: Sized,
        F: FnMut(&Acc, Self::Item) -> Option<(Acc, B)>,
    {
        MapAccum::new(self, accu, f)
    }

    /// Function to collect a nested [`Iterator`] into a [`Vec`] of `Vec`.
    fn collect2<I>(&mut self) -> Vec<Vec<I>>
    where
        Self::Item: Iterator<Item = I>,
    {
        self.map(|x| x.collect()).collect()
    }
}

impl<T: ?Sized> ExtraIterators for T where T: Iterator {}

/// Apply a function to the first element of a tuple, leaving the second element as is.
pub fn fst<A, B, C, F>(func: F, (a, b): (A, B)) -> (C, B)
where
    F: Fn(A) -> C,
{
    (func(a), b)
}

/// Apply a function to the second element of a tuple, leaving the first element as is.
pub fn snd<A, B, C, F>(func: F, (a, b): (A, B)) -> (A, C)
where
    F: Fn(B) -> C,
{
    (a, func(b))
}

/// Apply a matrix-transpose on a vector of vectors.
pub fn transpose<T>(v: Vec<Vec<T>>) -> Vec<Vec<T>> {
    assert!(!v.is_empty());
    let len = v[0].len();
    let mut iters: Vec<_> = v.into_iter().map(|n| n.into_iter()).collect();
    (0..len)
        .map(|_| {
            iters
                .iter_mut()
                .map(|n| n.next().unwrap())
                .collect::<Vec<T>>()
        })
        .collect()
}

/// Add an item to a vector, returning the extended vector.
pub fn cons<A>(vec: Vec<A>, item: A) -> Vec<A> {
    let mut vec = vec;
    vec.push(item);
    vec
}

/// Print a matrix display, as used in AoC 2022 day 10.
pub fn print_display(display: Vec<Vec<bool>>) {
    display.into_iter().for_each(|row| {
        println!(
            "{}",
            row.into_iter()
                .map(|c| if c { '#' } else { '.' })
                .collect::<String>()
        )
    });
    println!();
}

/// Convert a matrix-display character to [`char`].
pub fn lookup_char(hash: usize) -> char {
    match hash {
        311928102 => 'A',
        244620583 => 'B',
        210797862 => 'C',
        244622631 => 'D',
        504413231 => 'E',
        34651183 => 'F',
        211191078 => 'G',
        311737641 => 'H',
        34636833 => 'I',
        211034376 => 'J',
        307334313 => 'K',
        504398881 => 'L',
        311731689 => 'M',
        311735657 => 'N',
        211068198 => 'O',
        34841895 => 'P',
        483697958 => 'Q',
        307338535 => 'R',
        210897190 => 'S',
        69273679 => 'T',
        211068201 => 'U',
        106210601 => 'V',
        318022953 => 'W',
        311630121 => 'X',
        35793193 => 'Y',
        504434959 => 'Z',
        _ => '?',
    }
}

/// Convert a matrix-display text to a [`String`]
pub fn read_display(display: Vec<Vec<bool>>) -> String {
    crate::prelude::transpose(
        display
            .into_iter()
            .map(|row| {
                row.chunks(5)
                    .map(|rowchunk| {
                        rowchunk
                            .iter()
                            .enumerate()
                            .map(|(pos, pix)| if *pix { 2_usize.pow(pos as u32) } else { 0 })
                            .sum::<usize>()
                    })
                    .collect()
            })
            .collect(),
    )
    .into_iter()
    .map(|digit| {
        digit
            .into_iter()
            .enumerate()
            .map(|(pos, sub)| sub * 32_usize.pow(pos as u32))
            .sum::<usize>()
    })
    .map(lookup_char)
    .collect()
}

/// 2D cartesian coordinate, with `y` direction increasing downward.
#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub struct Coord {
    pub x: i32,
    pub y: i32,
}

impl Coord {
    /// Origin at (0, 0).
    pub const ORIGIN: Coord = Coord::new(0, 0);

    /// Constructor for `Coord`.
    pub const fn new(x: i32, y: i32) -> Coord {
        Coord { x, y }
    }

    /// Extract `x` component.
    pub fn x(self) -> i32 {
        self.x
    }

    /// Extract `x` component as [`usize`].
    pub fn x_as_usize(self) -> usize {
        self.x
            .try_into()
            .unwrap_or_else(|_| panic!("Error converting {:?} to unsigned", self))
    }

    /// Extract `y` component.
    pub fn y(self) -> i32 {
        self.y
    }

    /// Extract `y` component as [`usize`].
    pub fn y_as_usize(self) -> usize {
        self.y
            .try_into()
            .unwrap_or_else(|_| panic!("Error converting {:?} to unsigned", self))
    }

    /// Create an [`Iterator`] of `Coord`s representing a vertical line with `x` for the horizontal
    /// coordinate and the vertical coordinate ranging from `start` to `end`.
    pub fn vline(x: i32, start: i32, end: i32) -> impl Iterator<Item = Coord> {
        (start..=end).map(move |y| Coord { x, y })
    }

    /// Create an [`Iterator`] of `Coord`s representing a horizontal line with `x` for the vertical
    /// coordinate and the horizontal coordinate ranging from `start` to `end`.
    pub fn hline(y: i32, start: i32, end: i32) -> impl Iterator<Item = Coord> {
        (start..=end).map(move |x| Coord { x, y })
    }

    /// Create an [`Iterator`] of `Coord`s between two coordinates, effectively drawing a rectangle
    /// between two corners.
    pub fn span(self, rhs: &Coord) -> impl Iterator<Item = Coord> {
        (self.x.min(rhs.x)..=self.x.max(rhs.x))
            .cartesian_product(self.y.min(rhs.y)..=self.y.max(rhs.y))
            .map(move |(x, y)| Coord { x, y })
    }

    /// Add a displacement to a coordinate, returning the translated [`Coord`].
    fn translate(self, delta_x: i32, delta_y: i32) -> Coord {
        Coord {
            x: self.x + delta_x,
            y: self.y + delta_y,
        }
    }

    /// Translate coordinate 1 unit up.
    pub fn up(self) -> Coord {
        self.translate(0, -1)
    }

    /// Translate coordinate 1 unit down.
    pub fn down(self) -> Coord {
        self.translate(0, 1)
    }

    /// Translate coordinate 1 unit left.
    pub fn left(self) -> Coord {
        self.translate(-1, 0)
    }

    /// Translate coordinate 1 unit right.
    pub fn right(self) -> Coord {
        self.translate(1, 0)
    }

    /// Translate coordinate 1 unit diagonally down and to the left.
    pub fn down_left(self) -> Coord {
        self.translate(-1, 1)
    }

    /// Translate coordinate 1 unit diagonally down and to the right.
    pub fn down_right(self) -> Coord {
        self.translate(1, 1)
    }

    /// Translate coordinate 1 unit diagonally up and to the left.
    pub fn up_left(self) -> Coord {
        self.translate(1, -1)
    }

    /// Translate coordinate 1 unit diagonally up and to the right.
    pub fn up_right(self) -> Coord {
        self.translate(-1, -1)
    }

    /// Return the $L_\infty$ norm between two coordinates, also known as the *Chebyshev* or *chessboard distance*.
    pub fn chebyshev(&self, tar: Coord) -> i32 {
        Chebyshev.distance(*self, tar)
    }

    /// Return the $L_1$ norm between two coordinates, also known as the *Manhattan* or *taxicab distance*.
    pub fn manhattan(&self, tar: Coord) -> i32 {
        Manhattan.distance(*self, tar)
    }
}

impl Add for Coord {
    type Output = Coord;
    fn add(self, rhs: Coord) -> Coord {
        Coord {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl Add<&Coord> for Coord {
    type Output = Coord;
    fn add(self, rhs: &Coord) -> Coord {
        Coord {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl std::ops::AddAssign for Coord {
    fn add_assign(&mut self, rhs: Self) {
        *self = Coord {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl Sub<Coord> for Coord {
    type Output = Coord;
    fn sub(self, rhs: Coord) -> Coord {
        Coord {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl Sub<&Coord> for Coord {
    type Output = Coord;
    fn sub(self, rhs: &Coord) -> Coord {
        Coord {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl std::ops::SubAssign for Coord {
    fn sub_assign(&mut self, rhs: Self) {
        *self = Coord {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

/// 3D cartesian coordinate.
#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub struct Coord3 {
    x: i32,
    y: i32,
    z: i32,
}

impl Coord3 {
    /// Origin at (0, 0, 0).
    pub const ORIGIN: Coord3 = Coord3::new(0, 0, 0);

    /// Constructor for `Coord3`.
    pub const fn new(x: i32, y: i32, z: i32) -> Coord3 {
        Coord3 { x, y, z }
    }

    /// Extract `x` component.
    pub fn x(self) -> i32 {
        self.x
    }

    /// Extract `x` component as [`usize`].
    pub fn x_as_usize(self) -> usize {
        self.x
            .try_into()
            .unwrap_or_else(|_| panic!("Error converting {:?} to unsigned", self))
    }

    /// Extract `y` component.
    pub fn y(self) -> i32 {
        self.y
    }

    /// Extract `y` component as [`usize`].
    pub fn y_as_usize(self) -> usize {
        self.y
            .try_into()
            .unwrap_or_else(|_| panic!("Error converting {:?} to unsigned", self))
    }

    /// Extract `z` component.
    pub fn z(self) -> i32 {
        self.z
    }

    /// Extract `z` component as [`usize`].
    pub fn z_as_usize(self) -> usize {
        self.z
            .try_into()
            .unwrap_or_else(|_| panic!("Error converting {:?} to unsigned", self))
    }

    /// Parse a `Coord3` from a `String` of the form `"x,y,z"`.
    pub fn from(line: String) -> Coord3 {
        let xyz = line
            .split(',')
            .map(|n| {
                n.parse()
                    .unwrap_or_else(|_| panic!("Error parsing coordinate {}!", n))
            })
            .collect::<Vec<_>>();
        Coord3 {
            x: xyz[0],
            y: xyz[1],
            z: xyz[2],
        }
    }
}

/// $L_1$ metric (as a taxicab).
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Manhattan;

/// $L_\infty$ metric (as the king moves in chess).
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Chebyshev;

/// Trait to abstract over a given discrete 2D metric. Implemented by [`Chebyshev`] and [`Manhattan`].
/// Does not include an Euclidean equivalent as this would require a continuous number type, which
/// is not generally seen in Advent of Code.
pub trait DiscreteMetric: Copy + Clone + PartialEq {
    fn distance(self, src: Coord, tar: Coord) -> i32;
}

impl DiscreteMetric for Manhattan {
    fn distance(self, src: Coord, tar: Coord) -> i32 {
        let d = src - tar;
        d.x.abs() + d.y.abs()
    }
}

impl DiscreteMetric for Chebyshev {
    fn distance(self, src: Coord, tar: Coord) -> i32 {
        let d = src - tar;
        d.x.abs().max(d.y.abs())
    }
}

/// A `struct` representing a circle based on a given metric. Circles in [`Manhattan`] correspond to
/// diamonds, whilst circles in [`Chebyshev`] correspond to squares.
#[derive(Clone, Debug, PartialEq)]
pub struct Circle<M> {
    centre: Coord,
    radius: i32,
    metric: M,
}

impl<M: DiscreteMetric> Circle<M> {
    /// Create a generalised [`Circle`] given a centre `Coord`, a radius and a metric.
    pub fn new(centre: Coord, radius: i32, metric: M) -> Circle<M> {
        Circle {
            centre,
            radius,
            metric,
        }
    }

    /// Extract circle radius.
    pub fn radius(&self) -> i32 {
        self.radius
    }

    /// Predicate to check whether a given coordinate is within the circle.
    pub fn contains(&self, point: &Coord) -> bool {
        self.metric.distance(self.centre, point.to_owned()) <= self.radius
    }

    /// Expand the circle radius by 1.
    pub fn grow(&self) -> Circle<M> {
        Circle {
            radius: self.radius + 1,
            ..self.clone()
        }
    }

    /// Coordinate of the westmost coordinate in the circle: $(-r, 0)$
    pub fn westmost(&self) -> Coord {
        self.centre.translate(-self.radius, 0)
    }

    /// Coordinate of the eastmost coordinate in the circle: $(r, 0)$
    pub fn eastmost(&self) -> Coord {
        self.centre.translate(self.radius, 0)
    }

    /// Coordinate of the northmost coordinate in the circle: $(0, -r)$
    pub fn northmost(&self) -> Coord {
        self.centre.translate(0, -self.radius)
    }

    /// Coordinate of the southmost coordinate in the circle: $(0, r)$
    pub fn southmost(&self) -> Coord {
        self.centre.translate(0, self.radius)
    }

    /// Check whether a vertical line at horizontal coordinate `x` intersects the circle.
    pub fn covers_x(&self, x: i32) -> bool {
        (self.centre.x - x).abs() <= self.radius
    }

    /// Check whether a horizontal line at vertical coordinate `y` intersects the circle.
    pub fn covers_y(&self, y: i32) -> bool {
        (self.centre.y - y).abs() <= self.radius
    }
}

impl Circle<Chebyshev> {
    /// Construct a circle using the [`Chebyshev`] norm.
    pub fn chebyshev(centre: Coord, radius: i32) -> Circle<Chebyshev> {
        Circle::new(centre, radius, Chebyshev)
    }
}

impl Circle<Manhattan> {
    /// Construct a circle using the [`Manhattan`] norm.
    pub fn manhattan(centre: Coord, radius: i32) -> Circle<Manhattan> {
        Circle::new(centre, radius, Manhattan)
    }

    /// Construct the boundary of the circle as a [`Vec`].
    pub fn boundary(&self) -> Vec<Coord> {
        (0..self.radius)
            .map(|s| {
                [
                    self.centre.translate(s, self.radius - s),
                    self.centre.translate(-s, self.radius - s),
                    self.centre.translate(s, s - self.radius),
                    self.centre.translate(-s, s - self.radius),
                ]
            })
            .collect::<Vec<_>>()
            .concat()
    }
}

/// A [`Circle`] is greater than another `Circle` if the latter is entirely contained within the
/// other.
impl<M: DiscreteMetric> PartialOrd for Circle<M> {
    fn partial_cmp(&self, rhs: &Circle<M>) -> Option<Ordering> {
        if self.centre == rhs.centre && self.radius == rhs.radius {
            Some(Equal)
        } else if self.clone().contains(&rhs.westmost())
            && self.clone().contains(&rhs.eastmost())
            && self.clone().contains(&rhs.northmost())
            && self.clone().contains(&rhs.southmost())
        {
            Some(Greater)
        } else if rhs.clone().contains(&self.westmost())
            && rhs.clone().contains(&self.eastmost())
            && rhs.clone().contains(&self.northmost())
            && rhs.clone().contains(&self.southmost())
        {
            Some(Less)
        } else {
            None
        }
    }
}

/// A `trait` to mark an object representable by a single ASCII [`char`], used as part of [`PrintAscii2d`].
pub trait Ascii2d {
    /// Convert a single cell to an ASCII-[`char`].
    fn to_char(i: Option<&Self>) -> char;
}

/// A `trait` to mark an object as printable using a 2D array of characters.
pub trait PrintAscii2d {
    /// Print a 2D map between `top`, `bottom`, `left` and `right`.
    fn print_map(&self, top: i32, bottom: i32, left: i32, right: i32);
}

impl<I: Ascii2d> PrintAscii2d for HashMap<Coord, I> {
    /// Printing is enabled using a feature, otherwise treat calls to `.print_map()` as no-op.

    #[cfg(not(feature = "print"))]
    fn print_map(&self, _: i32, _: i32, _: i32, _: i32) {}

    #[cfg(feature = "print")]
    fn print_map(&self, top: i32, bottom: i32, left: i32, right: i32) {
        {
            (top..=bottom).for_each(|y| {
                (left..=right)
                    .for_each(|x| print!("{}", Ascii2d::to_char(self.get(&Coord::new(x, y)))));
                println!();
            });
            println!();
        }
    }
}

/// Add two values contained in [`Option`], as `liftA2 (+)`
pub fn oplus<T: Add>(a: Option<T>, b: Option<T>) -> Option<<T as Add>::Output> {
    a.zip(b).map(|(a, b)| a + b)
}

/// Quadratic formula for [`f64`]. Calculates the zeroes for a parabola described by $ax^2 +
/// bx + c$ or the intersection $ax^2 + bx = -c$.
/// $$\frac{-b \pm \sqrt{b^2 + 4ac}}{2a}$$
pub fn quadratic_formula(a: f64, b: f64, c: f64) -> (f64, f64) {
    let common = (b * b - 4. * a * c).sqrt();
    ((-b + common) / 2. * a, (-b - common) / 2. * a)
}

/// Combine a set of $[2-4]$ two-dimensional grids using a binary, ternary or quartenary function.
/// ```
/// # use aoc::map2d;
/// # use aoc::prelude::ExtraIterators;
///
/// let a = vec![vec![1, 2, 3], vec![4, 5, 6]];
/// let b = vec![vec![10, 5, 0], vec![-5, 0, 5]];
/// let c = vec![vec![false, true, false], vec![true, false, true]];
///
/// let result: Vec<Vec<i8>> =
///     map2d!(@closure (a, b, c) => ( if c { a * b } else { a } ),
///         a.into_iter(),
///         b.into_iter(),
///         c.into_iter())
///     .collect2();
///
/// assert_eq!(result, vec![vec![1, 10, 3], vec![-20, 5, 30]]);
/// ```
#[macro_export]
macro_rules! map2d {
    ( @closure $p:pat => $tup:expr , $first:expr, $second:expr) => {
        itertools::izip!($first, $second).map(|(a, b)| itertools::izip!(a, b).map(|$p| $tup))
    };

    ( @closure $p:pat => $tup:expr , $first:expr, $second:expr, $third:expr) => {
        itertools::izip!($first, $second, $third)
            .map(|(a, b, c)| itertools::izip!(a, b, c).map(|$p| $tup))
    };

    ( @closure $p:pat => $tup:expr , $first:expr, $second:expr, $third:expr, $fourth:expr ) => {
        itertools::izip!($first, $second, $third, $fourth)
            .map(|(a, b, c, d)| itertools::izip!(a, b, c, d).map(|$p| $tup))
    };
}
