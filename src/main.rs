#![allow(non_snake_case)]

use inquire::MultiSelect;

fn run_day(day: &u32) {
    match day {
        1 => aoc::days::day1::main(),
        2 => aoc::days::day2::main(),
        3 => aoc::days::day3::main(),
        4 => aoc::days::day4::main(),
        5 => aoc::days::day5::main(),
        6 => aoc::days::day6::main(),
        7 => aoc::days::day7::main(),
        8 => aoc::days::day8::main(),
        9 => aoc::days::day9::main(),
        10 => aoc::days::day10::main(),
        11 => aoc::days::day11::main(),
        12 => aoc::days::day12::main(),
        13 => aoc::days::day13::main(),
        14 => aoc::days::day14::main(),
        15 => aoc::days::day15::main(),
        16 => aoc::days::day16::main(),
        17 => aoc::days::day17::main(),
        18 => aoc::days::day18::main(),
        19 => aoc::days::day19::main(),
        20 => aoc::days::day20::main(),
        21 => aoc::days::day21::main(),
        22 => aoc::days::day22::main(),
        23 => aoc::days::day23::main(),
        24 => aoc::days::day24::main(),
        25 => aoc::days::day25::main(),
        _ => panic!("No such day."),
    }
}

fn main() {
    if std::env::args().len() > 1 {
        std::env::args()
            .skip(1)
            .for_each(|d| run_day(&d.parse::<u32>().unwrap_or(0)));
    } else {
        MultiSelect::new("SELECT DAY", (1..26).collect())
            .prompt()
            .expect("No result")
            .iter()
            .for_each(run_day);
    }
}
