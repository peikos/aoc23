//! Testing and timing framework.

use std::fmt::Debug;
use std::fs::File;
use std::io::Write;
use std::path::{Path, PathBuf};
use std::time::Instant;

#[cfg(not(feature = "timing"))]
use itertools::izip;

/// Valid `TestData` may be a single `&str` or two `&str`s - one for each part. Used to define
/// sample test data from each challenge within the source file.
pub trait TestData {
    fn first(&self) -> &str;
    fn second(&self) -> &str;
}

impl TestData for &str {
    fn first(&self) -> &str {
        self
    }

    fn second(&self) -> &str {
        self
    }
}

impl TestData for [&str; 2] {
    fn first(&self) -> &str {
        self[0]
    }

    fn second(&self) -> &str {
        self[1]
    }
}

/// Load input from file and provide result as [`String`]. If the file is not present, an attempt
/// is made to download it first.
pub fn load_input(day: u8) -> Result<String, anyhow::Error> {
    let mut path: PathBuf = ["data"].iter().collect();
    path.push(day.to_string());
    path.push("input");

    if !Path::new(&path).exists() {
        print!("Attempting to download input ... ");

        let jar = reqwest::cookie::Jar::default();
        let url = "https://adventofcode.com".parse()?;
        jar.add_cookie_str(dotenv!("SESSION"), &url);

        let client = reqwest::blocking::ClientBuilder::new()
            .cookie_store(true)
            .cookie_provider(std::sync::Arc::new(jar))
            .build()?;

        let url = url.join(&format!("{}/day/{}/input", dotenv!("YEAR"), &day))?;

        let resp = client
            .get(url)
            .send()?
            .error_for_status()
            .expect("Server error - is this day's input available yet?")
            .text()?;
        let mut file = File::create(&path)?;
        file.write_all(resp.as_bytes())?;
        println!("[32mdone![0m");
    }

    Ok(std::fs::read_to_string(&path)?)
}

/// Run both parts for a day and time the result, once. Requires the number of the `day`, the
/// `TestData` which is either a reference to `str` or `[str; 2]`, the `preprocess`, `part1` and
/// `part2` functions and an array with precomputed answers for validation.
#[cfg(not(feature = "timing"))]
pub fn run<I, R, Pre, F1, F2, T>(
    day: u8,
    test: T,
    preprocess: Pre,
    part1: F1,
    part2: F2,
    outputs: [Option<R>; 4],
) -> Result<(), anyhow::Error>
where
    I: Debug,
    Pre: Fn(&str) -> I,
    T: TestData,
    F1: Fn(&I) -> Option<R>,
    F2: Fn(&I) -> Option<R>,
    R: Debug + PartialEq + Eq + Clone,
{
    if cfg!(feature = "test") {
        let test1 = preprocess(test.first());
        let test2 = preprocess(test.second());
        let results = [part1(&test1), part2(&test2)];
        dbg!(&results);
        std::process::exit(0);
    }

    let start = Instant::now();

    let test1 = preprocess(test.first());
    let test2 = preprocess(test.second());
    let data = preprocess(&load_input(day)?);

    let results = [part1(&test1), part1(&data), part2(&test2), part2(&data)];
    let duration = start.elapsed();

    println!("[34m┌────────┐");
    println!("│ DAY {: <2?} │", day);
    println!("├────────┼─────────────────────────┬─────┐[0m");

    izip!(["Test 1", "Data 1", "Test 2", "Data 2"], results, outputs).for_each(
        |(label, result, output)| {
            if let Some(expected) = output {
                if let Some(calculated) = result {
                    if calculated == expected {
                        println!(
                            "[34m│ {: <6} │ {: <24?}│ [32m OK [34m│[0m",
                            label, calculated
                        );
                    } else {
                        println!(
                            "[34m│ {: <6} │ [31m{: <24}[34m│ [31mERR [34m│[0m",
                            label,
                            format!("{:?} ≠ {:?}", calculated, expected)
                        );
                    }
                } else {
                    println!(
                        "[34m│ {: <6} │                         │ [34mN/A [34m│[0m",
                        label
                    );
                }
            } else if let Some(calculated) = result {
                println!(
                    "[34m│ {: <6} │ {: <24?}│ [33mNEW [34m│[0m",
                    label, calculated
                );
            }
        },
    );
    println!("[34m└────────┴─────────────────────────┴─────┘[0m");
    println!();
    println!("Total running time: {:?}", duration);
    Ok(())
}

/// Timing version of `run()`. Run both parts for a day on input data only, and time the result.
/// The result is timed by first running the calculation $100$ times for caching, followed by
/// $1000$ runs to average for the result. Requires the number of the `day`, the `TestData` which
/// is either a reference to `str` or `[str; 2]`, the `preprocess`, `part1` and `part2` functions
/// and an array with precomputed answers for validation.
#[cfg(feature = "timing")]
pub fn run<I, R, Pre, F1, F2, T>(
    day: u8,
    _test: T,
    preprocess: Pre,
    part1: F1,
    part2: F2,
    _outputs: [Option<R>; 4],
) -> Result<(), anyhow::Error>
where
    Pre: Fn(&str) -> I,
    T: TestData,
    F1: Fn(&I) -> Option<R>,
    F2: Fn(&I) -> Option<R>,
    R: Debug + PartialEq + Eq,
{
    let raw = load_input(day)?;
    let pre = Instant::now();
    let data = preprocess(&raw);
    let duration_pre = pre.elapsed();

    (0..100).for_each(|_| {
        _ = part1(&data);
        _ = part2(&data);
    });

    let start = Instant::now();
    (0..1000).for_each(|_| {
        _ = part1(&data);
    });
    let duration1 = start.elapsed();

    let start = Instant::now();
    (0..1000).for_each(|_| {
        _ = part2(&data);
    });
    let duration2 = start.elapsed();

    println!("┌ DAY {: >2} ┐", day);
    println!("├────────┴─────────────┬──────────────────────┬──────────────────────┬─────────────────────┐");
    println!(
        "│ input:  {: >12?} │ part 1: {: >12?} │ part 2: {: >12?} │ total: {: >12?} │",
        duration_pre,
        duration1 / 1000,
        duration2 / 1000,
        (duration1 + duration2) / 1000 + duration_pre
    );
    println!("└──────────────────────┴──────────────────────┴──────────────────────┴─────────────────────┘");
    println!("");
    Ok(())
}

/// Macro to clean up the `main()` function for each day. Can take up to four answers in addition
/// to the day number.
///
/// ```
/// # use aoc::run;
/// # use aoc::days::day2::{read_data, first_part, second_part};
/// # const TEST: &str = "Game 0: 42 red" ;
/// // TEST, read_data, first_part and second_part are in scope.
/// run!(2 =>);            // will run day 2 with no precomputed answers.
/// run!(2 => 1, 2, 3, 4); // will run day 2 with all precomputed answers.
/// ```
///
/// # Answers for validation
/// Answers are provided sequentially, and should be ordered as such:
/// - Test answer part 1
/// - Real answer part 1
/// - Test answer part 2
/// - Real answer part 2
#[macro_export]
macro_rules! run {
    ( $day:expr ) => {
        $crate::infra::run(
            $day,
            TEST,
            read_data,
            first_part,
            second_part,
            [None, None, None, None],
        )
        .expect(&format!("Failed to run {}", $day))
    };
    ( $day:expr => ) => {
        $crate::infra::run(
            $day,
            TEST,
            read_data,
            first_part,
            second_part,
            [None, None, None, None],
        )
        .expect(&format!("Failed to run {}", $day))
    };
    ( $day:expr => $test1:expr ) => {
        $crate::infra::run(
            $day,
            TEST,
            read_data,
            first_part,
            second_part,
            [Some($test1), None, None, None],
        )
        .expect(&format!("Failed to run {}", $day))
    };
    ( $day:expr => $test1:expr, $answer1:expr ) => {
        $crate::infra::run(
            $day,
            TEST,
            read_data,
            first_part,
            second_part,
            [Some($test1), Some($answer1), None, None],
        )
        .expect(&format!("Failed to run {}", $day))
    };
    ( $day:expr => $test1:expr, $answer1:expr, $test2:expr ) => {
        $crate::infra::run(
            $day,
            TEST,
            read_data,
            first_part,
            second_part,
            [Some($test1), Some($answer1), Some($test2), None],
        )
        .expect(&format!("Failed to run {}", $day))
    };
    ( $day:expr => $test1:expr, $answer1:expr, $test2:expr, $answer2:expr ) => {
        $crate::infra::run(
            $day,
            TEST,
            read_data,
            first_part,
            second_part,
            [Some($test1), Some($answer1), Some($test2), Some($answer2)],
        )
        .expect(&format!("Failed to run {}", $day))
    };
}
