use crate::prelude::ExtraIterators;

const TEST: &str = "0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45";

type Intermediate = Vec<Vec<i32>>;

pub fn read_data(input: &str) -> Intermediate {
    input
        .lines()
        .map(|line| {
            line.split_whitespace()
                .map(|num| num.parse::<i32>().unwrap())
                .collect::<Vec<i32>>()
        })
        .collect::<Vec<_>>()
}

#[allow(clippy::ptr_arg)]
fn next(series: &Vec<i32>) -> i32 {
    let diffs = series.iter().zip(series.iter().skip(1)).map(|(a, b)| b - a);
    series.iter().last().unwrap()
        + if let Some(delta) = diffs.clone().singular() {
            delta
        } else {
            next(&diffs.collect::<Vec<_>>())
        }
}

#[allow(clippy::ptr_arg)]
fn prev(series: &Vec<i32>) -> i32 {
    let diffs = series.iter().zip(series.iter().skip(1)).map(|(a, b)| b - a);
    series.iter().next().unwrap()
        - if let Some(delta) = diffs.clone().singular() {
            delta
        } else {
            prev(&diffs.collect::<Vec<_>>())
        }
}

pub fn first_part(series: &Intermediate) -> Option<i32> {
    Some(series.iter().map(next).sum::<i32>())
}

pub fn second_part(series: &Intermediate) -> Option<i32> {
    Some(series.iter().map(prev).sum::<i32>())
}

pub fn main() {
    run!(9 => 114, 1938800261, 2, 1112)
}
