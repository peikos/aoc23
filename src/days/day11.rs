const TEST: &str = "
";

type Intermediate = ();

pub fn read_data(_input: &str) -> Intermediate {}

pub fn first_part(_: &Intermediate) -> Option<u32> {
    None
}

pub fn second_part(_: &Intermediate) -> Option<u32> {
    None
}

pub fn main() {
    run!(11 => )
}
