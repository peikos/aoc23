const TEST: &str = "Time:      7  15   30
Distance:  9  40  200";

type Intermediate = String;

pub fn read_data(input: &str) -> Intermediate {
    input.to_string()
}

fn intersect(pair: Vec<f64>) -> u32 {
    if pair.len() == 2 {
        let (up, down) = crate::prelude::quadratic_formula(-1., pair[0], -pair[1]);

        if up.ceil() == up {
            // Integer solutions
            (down - up - 1.) as u32
        } else {
            (down.floor() - up.floor()) as u32
        }
    } else {
        panic!("Pair isn't two!")
    }
}

pub fn first_part(input: &Intermediate) -> Option<u32> {
    input
        .lines()
        .map(|line| {
            line[10..]
                .split_whitespace()
                .map(|s| s.parse::<f64>().ok())
                .collect()
        })
        .collect::<Option<Vec<Vec<f64>>>>()
        .map(|s| {
            crate::prelude::transpose(s)
                .into_iter()
                .map(intersect)
                .product()
        })
}

pub fn second_part(input: &Intermediate) -> Option<u32> {
    input
        .lines()
        .map(|line| {
            line[10..]
                .chars()
                .filter(|x| x.is_ascii_digit())
                .collect::<String>()
                .parse::<f64>()
                .ok()
        })
        .collect::<Option<Vec<f64>>>()
        .map(intersect)
}

pub fn main() {
    run!(6 => 288, 227850, 71503, 42948149)
}
