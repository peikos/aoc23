use std::collections::HashSet;

const TEST: &str = "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11
";

#[derive(Debug, Clone)]
pub struct Card {
    winning: HashSet<u16>,
    numbers: HashSet<u16>,
}

impl Card {
    fn winning_numbers(&self) -> usize {
        self.winning.intersection(&self.numbers).count()
    }
}

type Intermediate = Vec<Card>;

pub fn read_data(lines: &str) -> Intermediate {
    lines
        .lines()
        .map(|line| {
            let mut halves = line[9..].split(|c: char| c == '|').map(|set| {
                set.split_whitespace()
                    .map(|s| s.parse::<u16>().expect("Failed to parse number"))
                    .collect::<HashSet<u16>>()
            });

            Card {
                winning: halves.next().expect("Missing line?"),
                numbers: halves.next().expect("Missing | symbol?"),
            }
        })
        .collect::<Vec<Card>>()
}

pub fn first_part(cards: &Intermediate) -> Option<u32> {
    Some(
        cards
            .iter()
            .map(|card| {
                let intersection = card.winning_numbers();
                if intersection > 0 {
                    2_i32.pow(intersection as u32 - 1) as u32
                } else {
                    0
                }
            })
            .sum(),
    )
}

pub fn second_part(cards: &Intermediate) -> Option<u32> {
    let mut stack = vec![1; cards.len()];

    cards.iter().enumerate().for_each(|(num, card)| {
        let multiplier = stack[num];

        (1..=card.winning_numbers()).for_each(|n| stack[num + n] += multiplier);
    });

    Some(stack.iter().sum())
}

pub fn main() {
    run!(4 => 13, 21088, 30, 6874754)
}
