use anyhow::Context;
use num::Integer;
use std::collections::HashMap;

const TEST: [&str; 2] = [
    "LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)
",
    "LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)
",
];

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Direction {
    Left,
    Right,
}

#[derive(Debug, Hash, PartialEq, Eq, Clone)]
pub struct Label(String);

#[derive(Debug)]
pub struct Map {
    path: Vec<Direction>,
    topology: HashMap<Label, (Label, Label)>,
}

type Intermediate = Map;

impl From<char> for Direction {
    fn from(c: char) -> Direction {
        match c {
            'L' => Direction::Left,
            'R' => Direction::Right,
            _ => panic!(),
        }
    }
}

impl From<&str> for Label {
    fn from(s: &str) -> Label {
        Label(s.to_string())
    }
}

impl Label {
    fn start(&self) -> bool {
        self.0.ends_with('A')
    }

    fn end(&self) -> bool {
        self.0.ends_with('Z')
    }
}

impl TryFrom<&str> for Map {
    type Error = anyhow::Error;

    fn try_from(s: &str) -> anyhow::Result<Map> {
        let mut topology = HashMap::new();
        let mut iter = s.lines();
        let path = iter
            .next()
            .context("No path line!")?
            .chars()
            .map(Direction::from)
            .collect::<Vec<_>>();
        iter.next();
        iter.map(|line| {
            let mut line = line.split_whitespace();
            let label = line.next().context("No label!")?.into();
            line.next();
            let left = line.next().context("No left target!")?[1..4].into(); // Slice to remove
            let right = line.next().context("No right target!")?[..3].into(); //  symbols
            topology.insert(label, (left, right));
            Ok(())
        })
        .collect::<anyhow::Result<Vec<()>>>()?;
        Ok(Map { path, topology })
    }
}

impl Map {
    fn find_cycle(&self, start: &Label, end: Option<&Label>) -> Option<u64> {
        let check_final = |node: &Label| {
            if let Some(end_node) = end {
                // Are we looking for a specific end node (ZZZ for part 1)
                node == end_node // then check whether the given node is equal
            } else {
                node.end() // else check whether it is an end node (ends in Z)
            }
        };

        self.path
            .clone()
            .into_iter()
            .cycle() // Repeat endlessly until stopped
            .scan((1, start), |st, step| {
                let node = &self.topology[st.1];
                let next = match step {
                    Direction::Left => &node.0,
                    Direction::Right => &node.1,
                };
                if check_final(next) {
                    // Stop on final node
                    None
                } else {
                    *st = (st.0 + 1, next); // Continue from next and increment count
                    Some(st.0)
                }
            })
            .collect::<Vec<_>>()
            .last() // Result is a counting list, only keep final
            .copied()
    }
}

pub fn read_data(input: &str) -> Intermediate {
    Map::try_from(input).expect("Failed to read input!")
}

pub fn first_part(input: &Intermediate) -> Option<u64> {
    input.find_cycle(&Label::from("AAA"), Some(&Label::from("ZZZ")))
}

pub fn second_part(input: &Intermediate) -> Option<u64> {
    input
        .topology
        .keys()
        .filter(|l| l.start())
        .map(|start| input.find_cycle(start, None))
        .fold(Some(1), |cur, step| Some(cur?.lcm(&step?)))
}

pub fn main() {
    run!(8 => 6, 19783, 6, 9177460370549)
}
