use crate::itertools::Itertools;
use anyhow::Context;

const TEST: &str = "32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483";

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Debug)]
enum Card {
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
    Ace,
    Joker,
}

#[derive(Clone, Debug)]
pub struct Hand {
    value: u32,
    joker_value: u32,
    bid: u16,
}

type Intermediate = Vec<Hand>;

impl TryFrom<char> for Card {
    type Error = anyhow::Error;

    fn try_from(s: char) -> anyhow::Result<Card> {
        use Card::*;
        match s {
            '2' => Ok(Two),
            '3' => Ok(Three),
            '4' => Ok(Four),
            '5' => Ok(Five),
            '6' => Ok(Six),
            '7' => Ok(Seven),
            '8' => Ok(Eight),
            '9' => Ok(Nine),
            'T' => Ok(Ten),
            'J' => Ok(Jack),
            'Q' => Ok(Queen),
            'K' => Ok(King),
            'A' => Ok(Ace),
            _ => Err(anyhow::Error::msg("Unparsable card!")),
        }
    }
}

impl Card {
    fn jack_to_joker(&mut self) {
        if *self == Card::Jack {
            *self = Card::Joker
        }
    }

    fn score(&self) -> u32 {
        use Card::*;
        match self {
            Two => 1,
            Three => 2,
            Four => 3,
            Five => 4,
            Six => 5,
            Seven => 6,
            Eight => 7,
            Nine => 8,
            Ten => 9,
            Jack => 10,
            Queen => 11,
            King => 12,
            Ace => 13,
            Joker => 0,
        }
    }
}

fn value(cards: &[Card], convert_jokers: bool) -> u32 {
    let cards = &mut cards.to_vec();

    if convert_jokers {
        cards.iter_mut().for_each(|c| c.jack_to_joker());
    }

    let jokers = cards.iter().filter(|c| **c == Card::Joker).count();

    let counts = cards
        .iter()
        .filter(|c| **c != Card::Joker)
        .sorted()
        .group_by(|x| *x)
        .into_iter()
        .map(|x| x.1.count())
        .collect::<Vec<usize>>();

    let max_count = counts.iter().max().unwrap_or(&0) + jokers;

    return if max_count == 5 {
        6 * 14_u32.pow(5)
    } else if max_count == 4 {
        5 * 14_u32.pow(5)
    } else if max_count == 3 && counts.iter().min().unwrap_or(&0) == &2 {
        // 3+2 for full house
        4 * 14_u32.pow(5)
    } else if max_count == 3 {
        3 * 14_u32.pow(5)
    } else if counts.iter().filter(|x| **x == 2).count() == 2 {
        // Only account for "natural" 2 pairs, as jokers are always better spent elsewhere
        2 * 14_u32.pow(5)
    } else if max_count == 2 {
        14_u32.pow(5)
    } else {
        0
    } + cards
        .iter()
        .rev()
        .enumerate()
        .map(|(pos, card)| card.score() * 14_u32.pow(pos as u32))
        .sum::<u32>();
}

impl TryFrom<&str> for Hand {
    type Error = anyhow::Error;

    fn try_from(input: &str) -> anyhow::Result<Hand> {
        let mut input = input.split_whitespace();

        let cards: Vec<Card> = input
            .next()
            .context("No hand information!")?
            .chars()
            .map(Card::try_from)
            .collect::<anyhow::Result<Vec<Card>>>()?;

        let bid = input
            .next()
            .context("No bid information!")?
            .parse::<u16>()?;

        Ok(Hand {
            value: value(&cards, false),
            joker_value: value(&cards, true),
            bid,
        })
    }
}

pub fn read_data(input: &str) -> Intermediate {
    input
        .lines()
        .map(Hand::try_from)
        .collect::<anyhow::Result<Vec<Hand>>>()
        .expect("Failed on input")
}

pub fn first_part(hands: &Intermediate) -> Option<u32> {
    Some(
        hands
            .iter()
            .sorted_by_key(|c| c.value)
            .enumerate()
            .map(|(rank, hand)| (1 + rank) as u32 * hand.bid as u32)
            .sum::<u32>(),
    )
}

pub fn second_part(hands: &Intermediate) -> Option<u32> {
    Some(
        hands
            .iter()
            .sorted_by_key(|c| c.joker_value)
            .enumerate()
            .map(|(rank, hand)| (1 + rank) as u32 * hand.bid as u32)
            .sum::<u32>(),
    )
}

pub fn main() {
    run!(7 => 6440, 245794640, 5905, 247899149 )
}
