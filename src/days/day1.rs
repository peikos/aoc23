use regex::Regex;

const TEST: [&str; 2] = [
    "1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet
",
    "two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen
",
];

type Intermediate = String;

// In this case, keep data mostly as is and only take ownership.
// This is needed for doing a single find-and-replace swoop for part 2.
pub fn read_data(lines: &str) -> Intermediate {
    String::from(lines)
}

pub fn first_part(calibration: &Intermediate) -> Option<u32> {
    calibration
        .lines()
        .map(|line| {
            let mut line = line.chars().filter(|c| c.is_numeric());
            let d1 = line.next()?;
            let d2 = line.last().unwrap_or(d1);
            Some(d1.to_digit(10)? * 10 + d2.to_digit(10)?)
        })
        .sum()
}

// Just find and replace all occurences of spelled-out digits in advance.
fn replace_digits(line: &str) -> String {
    let mut line = line.to_owned();
    // Loop over each digit, replacing it with a numeric value. Keep letters that might overlap
    // with another spelled-out digit.
    for (text, digit) in [
        ("one", "o1e"),
        ("two", "t2o"),
        ("three", "t3e"),
        ("four", "4"),
        ("five", "5e"),
        ("six", "6"),
        ("seven", "7n"),
        ("eight", "e8t"),
        ("nine", "n9e"),
    ] {
        line = Regex::new(text)
            .unwrap()
            .replace_all(&line, digit)
            .into_owned();
    }
    line
}

pub fn second_part(calibration: &Intermediate) -> Option<u32> {
    first_part(&replace_digits(calibration))
}

pub fn main() {
    run!(1 => 142, 54304, 281, 54418)
}
