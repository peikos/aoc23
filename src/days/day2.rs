use std::ops::Add;

use crate::pest::Parser;
use crate::prelude::oplus;

const TEST: &str = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
";

type Intermediate = Vec<Game>;

#[derive(Parser)]
#[grammar = "days/day2.pest"]
struct Day2Parser;

#[derive(Debug)]
pub struct ColourSet {
    red: usize,
    green: usize,
    blue: usize,
}

#[derive(Debug)]
pub struct Game {
    id: usize,
    cubes: Vec<ColourSet>,
}

impl Add for ColourSet {
    type Output = ColourSet;

    fn add(self, rhs: ColourSet) -> ColourSet {
        ColourSet {
            red: self.red + rhs.red,
            green: self.green + rhs.green,
            blue: self.blue + rhs.blue,
        }
    }
}

impl ColourSet {
    const EMPTY: ColourSet = ColourSet {
        red: 0,
        green: 0,
        blue: 0,
    };

    pub fn from_red(red: usize) -> ColourSet {
        ColourSet {
            red,
            green: 0,
            blue: 0,
        }
    }
    pub fn from_green(green: usize) -> ColourSet {
        ColourSet {
            red: 0,
            green,
            blue: 0,
        }
    }
    pub fn from_blue(blue: usize) -> ColourSet {
        ColourSet {
            red: 0,
            green: 0,
            blue,
        }
    }

    /// Least upper bound of two sets of cubes.
    pub fn join(&self, rhs: &ColourSet) -> ColourSet {
        ColourSet {
            red: self.red.max(rhs.red),
            green: self.green.max(rhs.green),
            blue: self.blue.max(rhs.blue),
        }
    }

    /// Convert to usize for part 2 answer, as per instructions.
    fn power(&self) -> usize {
        self.red * self.green * self.blue
    }

    /// Convert parse-tree for a single round, adding individual colour counts.
    fn read_round(iterator: pest::iterators::Pair<Rule>) -> Option<ColourSet> {
        iterator
            .into_inner()
            .map(ColourSet::read_subset)
            .fold(Some(ColourSet::EMPTY), oplus)
    }

    /// Convert parse-tree for a single colour×count pair, selecting appropriate constructor based
    /// on string value.
    fn read_subset(iterator: pest::iterators::Pair<Rule>) -> Option<ColourSet> {
        let mut set = iterator.into_inner();

        let number = set.next()?.as_str().parse().ok()?;
        let colour = set.next()?.as_str();

        Some(match colour {
            "red" => ColourSet::from_red(number),
            "green" => ColourSet::from_green(number),
            "blue" => ColourSet::from_blue(number),
            _ => panic!(),
        })
    }
}

impl Game {
    /// Least upper bound of all sets in a game.
    fn join(&self) -> ColourSet {
        self.cubes.iter().fold(ColourSet::EMPTY, |a, b| a.join(b))
    }

    /// Returns the id of a game if valid; otherwise None. Use with filter_map()
    fn valid_id(&self) -> Option<usize> {
        if self.valid() {
            Some(self.id)
        } else {
            None
        }
    }

    /// As per instructions, game is valid if it can be played with the given set of cubes
    /// available.
    fn valid(&self) -> bool {
        self.cubes
            .iter()
            .all(|round| round.red <= 12 && round.green <= 13 && round.blue <= 14)
    }

    /// Parse a single game.
    fn read(iterator: pest::iterators::Pair<Rule>) -> Option<Game> {
        let mut game = iterator.into_inner();

        let id = game.next()?.as_str().parse().ok()?;
        let cubes = game
            .map(ColourSet::read_round)
            .collect::<Option<Vec<ColourSet>>>()?;

        Some(Game { id, cubes })
    }
}

/// Read input file to parse and convert each game (line).
pub fn read_data(lines: &str) -> Intermediate {
    Day2Parser::parse(Rule::result, lines)
        .expect("No parse!")
        .next()
        .expect("Empty iterator")
        .into_inner()
        .map(Game::read)
        .collect::<Option<Vec<Game>>>()
        .expect("Failed to read input")
}

/// Filter out valid games and add their IDs.
pub fn first_part(i: &Intermediate) -> Option<usize> {
    Some(i.iter().filter_map(|g| g.valid_id()).sum())
}

/// Find the least upper bound of all colours for each game, take their product and sum the
/// results.
pub fn second_part(i: &Intermediate) -> Option<usize> {
    Some(i.iter().map(|g| g.join().power()).sum())
}

pub fn main() {
    run!(2 => 8, 2541, 2286, 66016)
}
