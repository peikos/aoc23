use std::collections::HashMap;
use crate::prelude::{Coord,PrintAscii2d,Ascii2d};

const TEST: [&str; 2] = ["7-F7-
.FJ|7
SJLL7
|F--J
LJ.LJ","FF7FSF7F7F7F7F7F---7
L|LJ||||||||||||F--J
FL-7LJLJ||||||LJL-77
F--JF--7||LJLJ7F7FJ-
L---JF-JLJ.||-FJLJJ7
|F|F-JF---7F7-L7L|7|
|FFJF7L7F-JF7|JL---7
7-L-JL7||F7|L7F-7F7|
L.L7LFJ|||||FJL7||LJ
L7JLJL-JLJLJL--JLJ.L"];

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum ComingFrom {
    Left,
    Right,
    Top,
    Bottom,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Cell {
    Start,
    Horizontal,
    Vertical,
    TopLeft,
    TopRight,
    BottomLeft,
    BottomRight,
    Empty
}

impl Cell {
    fn accessible_from_left(&self) -> bool {
        [Cell::Horizontal, Cell::TopLeft, Cell::BottomLeft].contains(self)
    }
    fn accessible_from_right(&self) -> bool {
        [Cell::Horizontal, Cell::TopRight, Cell::BottomRight].contains(self)
    }
    fn accessible_from_top(&self) -> bool {
        [Cell::Vertical, Cell::TopLeft, Cell::TopRight].contains(self)
    }
    fn accessible_from_bottom(&self) -> bool {
        [Cell::Vertical, Cell::BottomLeft, Cell::BottomRight].contains(self)
    }

    fn go(&self, coord: Coord, from: ComingFrom) -> (Coord, ComingFrom) {
        //dbg!((&self, &coord, &from));
        match self {
            Cell::Horizontal => if from == ComingFrom::Left { (coord.right(), ComingFrom::Left) } else { (coord.left(), ComingFrom::Right) },
            Cell::Vertical => if from == ComingFrom::Top { (coord.down(), ComingFrom::Top) } else { (coord.up(), ComingFrom::Bottom) },
            Cell::TopLeft => if from == ComingFrom::Top { (coord.left(), ComingFrom::Right) } else { (coord.up(), ComingFrom::Bottom) },
            Cell::BottomLeft => if from == ComingFrom::Bottom { (coord.left(), ComingFrom::Right) } else { (coord.down(), ComingFrom::Top) },
            Cell::TopRight => if from == ComingFrom::Top { (coord.right(), ComingFrom::Left) } else { (coord.up(), ComingFrom::Bottom) },
            Cell::BottomRight => if from == ComingFrom::Bottom { (coord.right(), ComingFrom::Left) } else { (coord.down(), ComingFrom::Top) },
            _ => panic!("{:?}, {:?}", self, from)

            }
    }
}

impl Ascii2d for Cell {
    fn to_char(i: Option<&Self>) -> char {
        if let Some(c) = i {
        match c {
            Cell::Empty => '.',
            Cell::Start => '▓',
            Cell::Horizontal => '═',
            Cell::Vertical => '║',
            Cell::TopLeft => '╝',
            Cell::TopRight => '╚',
            Cell::BottomLeft => '╗',
            Cell::BottomRight => '╔',
        }
        } else { ' ' }
    }
}

type Intermediate = (Coord, HashMap<Coord, Cell>);

// TODO: CoordGrid<T: TryFrom<Char>> struct impl
pub fn read_data(input: &str) -> Intermediate {
        let mut map: HashMap<Coord, Cell> = HashMap::new();
        let mut start = Coord::ORIGIN;

        //dbg!(input.lines().count(), input.lines().next().unwrap().chars().count());
        input.lines().enumerate().for_each(|(y, row)| {
            row.bytes().enumerate().for_each(|(x, c)| {
                map.insert(
                    Coord::new(x as i32, y as i32),
                    if c == 83 {
                        // Ascii S
                        start = Coord::new(x as i32, y as i32); // TODO parameterise over int type
                        //c.into() // TODO try_into
                        Cell::Start
                    } else if c == 45 {
                        // Ascii -
                        Cell::Horizontal
                    } else if c == 124 {
                        // Ascii |
                        Cell::Vertical
                    } else if c == 55 {
                        // Ascii 7
                        Cell::BottomLeft
                    } else if c == 74 {
                        // Ascii J
                        Cell::TopLeft
                    } else if c == 76 {
                        // Ascii L
                        Cell::TopRight
                    } else if c == 70 {
                        // Ascii F
                        Cell::BottomRight
                    } else if c == 46 {
                        // Ascii .
                        Cell::Empty
                    } else {
                        panic!("Invalid character in input!")
                    },
                );
            })
        });


    let start_type =
    if let Some(left) = map.get(&start.left()) && left.accessible_from_right() {
        if let Some(up) = map.get(&start.up()) && up.accessible_from_bottom() {
            Cell::TopLeft
        } else if let Some(down) = map.get(&start.down()) && down.accessible_from_top() {
            Cell::BottomLeft
        } else {
            panic!()
        }
    } else if let Some(right) = map.get(&start.right()) && right.accessible_from_left() {
        if let Some(up) = map.get(&start.up()) && up.accessible_from_bottom() {
            Cell::TopRight
        } else if let Some(down) = map.get(&start.down()) && down.accessible_from_top() {
            Cell::BottomRight
        } else {
            panic!()
        }
    } else {
        panic!()
    };

    map.get_mut(&start).map(|val| *val = start_type);
        (start, map)
}

pub fn first_part((start, map): &Intermediate) -> Option<u32> {
    //dbg!(&map);
    map.print_map(0,140,0,140);

    let (first, from) = 
    if let Some(left) = map.get(&start.left()) && left.accessible_from_right() {
        (start.left(), ComingFrom::Right)
    } else if let Some(right) = map.get(&start.right()) && right.accessible_from_left() {
        (start.right(), ComingFrom::Left)
    } else if let Some(up) = map.get(&start.up()) && up.accessible_from_bottom() {
        (start.up(), ComingFrom::Bottom)
    } else if let Some(down) = map.get(&start.down()) && down.accessible_from_top() {
        (start.down(), ComingFrom::Top)
    } else {
        panic!()
    };

    itertools::unfold((first, from, 1), |(coord, dir, count)| {
        if *coord == *start {
            None
        } else {
            let (next, trail) = map[coord].go(*coord, *dir);
            *coord = next;
            *dir = trail;
            *count += 1;
            Some(*count)
        }
    }).last().map(|v| v / 2)
}

pub fn second_part((start, map): &Intermediate) -> Option<u32> {

    // find_first
    let (first, from) = 
    if let Some(left) = map.get(&start.left()) && left.accessible_from_right() {
        (start.left(), ComingFrom::Right)
    } else if let Some(right) = map.get(&start.right()) && right.accessible_from_left() {
        (start.right(), ComingFrom::Left)
    } else if let Some(up) = map.get(&start.up()) && up.accessible_from_bottom() {
        (start.up(), ComingFrom::Bottom)
    } else if let Some(down) = map.get(&start.down()) && down.accessible_from_top() {
        (start.down(), ComingFrom::Top)
    } else {
        panic!()
    };

    // TODO - function

    let mut cycle = itertools::unfold((first, from, 1), |(coord, dir, count)| {
        if *coord == *start {
            None
        } else {
            let (next, trail) = map[coord].go(*coord, *dir);
            *coord = next;
            *dir = trail;
            *count += 1;
            Some(*coord)
        }
    }).filter(|x| map[x] != Cell::Horizontal).collect::<Vec<_>>(); // TODO sort later?
    //cycle.push(*start);
    cycle.push(first);
    //cycle.sort_by_key(|c| c.y());
    //dbg!(&cycle[cycle.len()-2], &cycle[cycle.len()-1], cycle[0]);




    // remove cycle from to be checked
    //let empty = map.iter().filter(|(_, cell)| *cell == &Cell::Empty).map(|(c, _)| c).collect::<Vec<_>>();
    let empty = map.keys().filter(|c| !cycle.contains(&c)).collect::<Vec<_>>();
    //dbg!(empty.len());





    let mut count = 0;
    empty.iter().for_each(|candidate| {
        let y = candidate.y();
        let range: Box<dyn Iterator<Item=i32>> = if candidate.x() < 70 { Box::new(0..=candidate.x()) } else { Box::new(candidate.x()..140) }; // TODO 140 uit struct

        let mut intersections = 0;
        let mut half: Option<Cell> = None;
        range.for_each(|x_ray| {
            let c = Coord::new(x_ray, y);
            if cycle.contains(&c) {
                match map.get(&c) {
                    Some(Cell::Vertical) => {intersections += 1},
                    Some(Cell::TopLeft) => { if half == Some(Cell::BottomRight) { intersections += 1; half = None; } else if half == Some(Cell::TopRight) { half = None } else { half = Some(Cell::TopLeft) }; }, 
                    Some(Cell::TopRight) => {  half = Some(Cell::TopRight) ; }, 
                    Some(Cell::BottomLeft) => { if half == Some(Cell::TopRight) { intersections += 1; half = None; } else if half == Some(Cell::BottomRight) { half = None }else { half = Some(Cell::BottomLeft) }; }, 
                    Some(Cell::BottomRight) => { half = Some(Cell::BottomRight) ; }, 
                    Some(Cell::Horizontal) => {},
                    _ => panic!("{:?}, {:?}", &c, map.get(&c))
                }
            }
        });
            //dbg!(&candidate, intersections, half);
            count += intersections % 2;

        //let mut row = cycle.iter()
            //.filter(|c| c.y() == candidate.y())
            //.map(|c| c.x())
            ////.sort()
            //.collect::<Vec<_>>();
        //row.sort();
        //if let Err(idx) = row.binary_search(&candidate.x()) {
            //dbg!((&candidate, &row, &idx));
            //if idx % 2 == 1 {
                //count += 1;
            //}
        //} else {
            //panic!()
        //}
    });


    // For each empty, take only coords on same line, sort by x and check isertion index
    //   - even -> outside, odd -> inside


    Some(count)
}

pub fn main() {
    run!(10 => 8, 6725)
}
