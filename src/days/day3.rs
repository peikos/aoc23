use itertools::Itertools;
use ndarray::{s, Array2};

const TEST: &str = "467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..
";

#[derive(Copy, Clone, Debug)]
pub struct Cell {
    character: char,
    keep: bool,
}

#[derive(Debug, Clone)]
pub struct Schematic(Array2<Cell>);

type Coord = (usize, usize);
type Gear = (Coord, Vec<Coord>);

impl Schematic {
    pub fn read(input: &str) -> Self {
        let rows = input.clone().lines().count();
        let cols = input.len() / rows;

        Schematic(
            Array2::from_shape_vec(
                (rows, cols),
                input
                    .chars()
                    .map(|x| Cell {
                        character: x,
                        keep: false,
                    })
                    .collect(),
            )
            .expect("Failed to convert input"),
        )
    }

    pub fn max_y(&self) -> usize {
        self.0.nrows() - 1
    }

    pub fn max_x(&self) -> usize {
        self.0.ncols() - 1
    }

    pub fn extract_labels(&self) -> Vec<Option<u32>> {
        self.0
            .iter()
            .map(|x| if x.keep { x.character } else { ' ' })
            .collect::<String>()
            .split_whitespace()
            .map(|s| s.parse::<u32>().ok())
            .collect()
    }

    pub fn window(&self, gear_y: &usize, gear_x: &usize) -> ndarray::Array2<Cell> {
        self.0
            .slice(s![gear_y - 1..=gear_y + 1, gear_x - 3..=gear_x + 4])
            .to_owned()
    }

    pub fn indexed_iter(&self) -> impl Iterator<Item = (Coord, &Cell)> {
        self.0.indexed_iter()
    }
}

impl std::ops::Index<Coord> for Schematic {
    type Output = Cell;

    fn index(&self, idx: Coord) -> &Cell {
        &self.0[idx]
    }
}

impl std::ops::IndexMut<Coord> for Schematic {
    fn index_mut(&mut self, idx: Coord) -> &mut Cell {
        &mut self.0[idx]
    }
}

/// Read data into 2D-array of characters and bools.
pub fn read_data(input: &str) -> Schematic {
    Schematic::read(input)
}

/// Mark a cell, and then recursively mark neighbouring digits.
fn flood(grid: &mut Schematic, (y, x): Coord) -> Vec<Coord> {
    let mut flooded = Vec::new();
    let max_x = grid.max_x();
    let max_y = grid.max_y();

    // Mark digits for later.
    if grid[(y, x)].character.is_ascii_digit() {
        grid[(y, x)].keep = true;
    }

    // Create moving window.
    let xrange = if x == 0 {
        x..=x + 1
    } else if x == max_x {
        x - 1..=x
    } else {
        x - 1..=x + 1
    };

    let yrange = if y == 0 {
        y..=y + 1
    } else if y == max_y {
        y - 1..=y
    } else {
        y - 1..=y + 1
    };

    // Recurse over all neighbour-cells that contain a digit and have not been visited.
    yrange
        .cartesian_product(xrange)
        .for_each(|(conv_y, conv_x)| {
            if !grid[(conv_y, conv_x)].keep && grid[(conv_y, conv_x)].character.is_ascii_digit() {
                flooded.append(&mut flood(grid, (conv_y, conv_x)));
            }
        });

    // Keep track of all cells filled for in part 2.
    flooded.push((y, x));
    flooded
}

/// Attempt to extract a gear at given location.
fn extract_gear(grid: &mut Schematic, (y, x): Coord) -> Option<Gear> {
    let max_y = grid.max_y();
    let max_x = grid.max_x();

    let mut to_flood = Vec::new();
    let mut above = 0;
    let mut left = 0;
    let mut right = 0;
    let mut below = 0;

    // Same as above, but now keep track of the number of neighbouring digits.
    if x > 0 && y > 0 && grid[(y - 1, x - 1)].character.is_ascii_digit() {
        to_flood.push((y - 1, x - 1));
        above = 1;
    }

    if y > 0 && grid[(y - 1, x)].character.is_ascii_digit() {
        to_flood.push((y - 1, x));
        above = 1;
    }

    // If the top-left and top-right are digits, but top centre is not, we have two separate
    // labels.
    if y > 0 && x < max_x && grid[(y - 1, x + 1)].character.is_ascii_digit() {
        to_flood.push((y - 1, x + 1));
        if grid[(y - 1, x - 1)].character.is_ascii_digit()
            && !grid[(y - 1, x)].character.is_ascii_digit()
        {
            above = 2;
        } else {
            above = 1;
        }
    }

    if x > 0 && grid[(y, x - 1)].character.is_ascii_digit() {
        to_flood.push((y, x - 1));
        left = 1;
    }

    if x < max_x && grid[(y, x + 1)].character.is_ascii_digit() {
        to_flood.push((y, x + 1));
        right = 1;
    }

    if y < max_y && x > 0 && grid[(y + 1, x - 1)].character.is_ascii_digit() {
        to_flood.push((y + 1, x - 1));
        below = 1;
    }

    if y < max_y && grid[(y + 1, x)].character.is_ascii_digit() {
        to_flood.push((y + 1, x));
        below = 1;
    }

    if y < max_y && x < max_x && grid[(y + 1, x + 1)].character.is_ascii_digit() {
        to_flood.push((y + 1, x + 1));
        if grid[(y + 1, x - 1)].character.is_ascii_digit()
            && !grid[(y + 1, x)].character.is_ascii_digit()
        {
            below = 2;
        } else {
            below = 1;
        }
    }

    // If a candidate gear has two numeric labels, flood mark the gear and note the digit
    // coordinates.
    if above + below + left + right == 2 {
        let coords = to_flood
            .into_iter()
            .flat_map(|(y, x)| flood(grid, (y, x)))
            .collect::<Vec<_>>();
        Some(((y, x), coords))
    } else {
        None
    }
}

/// Part 1: Extract all labels corresponding to parts.
pub fn first_part(input: &Schematic) -> Option<u32> {
    let mut grid = input.clone();

    // Traverse grid and flood-mark each symbol.
    input.indexed_iter().for_each(|((y, x), p)| {
        let v = p.character;
        if !p.keep && v != '.' && v != '\n' && !v.is_ascii_digit() {
            // Symbols get flooded, recursively applying to adjacent numbers.
            flood(&mut grid, (y, x));
        }
    });

    grid.extract_labels().into_iter().sum()
}

/// Part 1: Extract all labels corresponding to gears: i.e. '*' with two labels.
pub fn second_part(input: &Schematic) -> Option<u32> {
    let mut grid = input.clone();

    // Attempt to extract gear information for every '*' symbol.
    let gears: Vec<_> = input
        .indexed_iter()
        .flat_map(|((y, x), p)| {
            if p.character == '*' {
                extract_gear(&mut grid, (y, x))
            } else {
                None
            }
        })
        .collect();

    // Slice a window containing the gear and the associated labels.
    gears
        .iter()
        .map(|((gear_y, gear_x), coords)| {
            let mut window = Schematic(Array2::from_elem(
                (3, 8),
                Cell {
                    character: ' ',
                    keep: true,
                },
            ));
            let orig = input.window(gear_y, gear_x);

            // Only copy over parts of window associated with gear labels to filter out parts of
            // adjacent labels.
            coords.iter().for_each(|(y, x)| {
                let rel_y = 1 + *y - gear_y;
                let rel_x = 3 + *x - gear_x;

                window[(rel_y, rel_x)].character = orig[(rel_y, rel_x)].character
            });

            // Sanity check: we should have exactly two numbers to multiply.
            assert_eq!(window.extract_labels().len(), 2);

            window.extract_labels().into_iter().product::<Option<u32>>()
        })
        .sum()
}

pub fn main() {
    run!(3 => 4361, 532331, 467835, 82301120)
}
