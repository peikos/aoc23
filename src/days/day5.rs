use rayon::prelude::*;

const TEST: &str = "seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4";

type Intermediate = (Vec<Seed>, Vec<Step>);

#[derive(Clone, Debug)]
pub struct Mapping {
    dest: u64,
    source: u64,
    range: u64,
}

//= Vec<u64>;
type Seed = u64;
type Step = Vec<Mapping>;

impl TryFrom<Vec<u64>> for Mapping {
    type Error = ();

    fn try_from(vec: Vec<u64>) -> Result<Mapping, ()> {
        if vec.len() != 3 {
            Err(())
        } else {
            Ok(Mapping {
                dest: vec[0],
                source: vec[1],
                range: vec[2],
            })
        }
    }
}

impl Mapping {
    fn translate(&self, from: u64) -> Option<u64> {
        if from >= self.source && from < self.source + self.range {
            Some(from - self.source + self.dest)
        } else {
            None
        }
    }
}

pub fn read_data(input: &str) -> Intermediate {
    let lines: Vec<String> = input.lines().map(String::from).collect();
    let mut iter = lines.split(|s| s == &String::from(""));
    let seeds = iter.next().expect("No seeds line")[0][6..]
        .split_whitespace()
        .map(|s| s.parse::<u64>().ok())
        .collect::<Option<Vec<u64>>>()
        .expect("Failed to parse seeds line");

    let steps = iter
        .map(|block| {
            let mut block = block.iter();
            block.next();
            block
                .map(|l| {
                    l.split_whitespace()
                        .map(|s| s.parse::<u64>().ok())
                        .collect::<Option<Vec<u64>>>()
                        .and_then(|s| Mapping::try_from(s).ok())
                })
                .collect::<Option<Step>>()
        })
        .collect::<Option<Vec<Step>>>()
        .expect("Failed to parse step");

    (seeds, steps)
}

fn seeds_to_location<I: ParallelIterator<Item = Seed>>(seeds: I, steps: &[Step]) -> Option<u64> {
    seeds
        .map(|seed| {
            steps.iter().fold(seed, |cur, step| {
                step.iter()
                    .flat_map(|mapping| mapping.translate(cur))
                    .next()
                    .unwrap_or(cur)
            })
        })
        .min()
}

pub fn first_part((seeds, steps): &Intermediate) -> Option<u64> {
    seeds_to_location(seeds.clone().into_par_iter(), steps)
}

pub fn second_part((seeds, steps): &Intermediate) -> Option<u64> {
    seeds_to_location(
        seeds
            .par_iter()
            .chunks(2)
            .flat_map(|x| (*x[0]..x[0] + x[1]).collect::<Vec<_>>()),
        steps,
    )
}

pub fn main() {
    run!(5 => 35, 88151870, 46, 2008785)
}
