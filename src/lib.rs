#![feature(iter_intersperse)]
#![feature(portable_simd)]
#![feature(let_chains)]

extern crate pest;
#[macro_use]
extern crate pest_derive;

#[macro_use]
extern crate dotenv_codegen;

#[macro_use]
extern crate itertools;

#[macro_use]
pub mod prelude;

#[macro_use]
pub mod infra;

pub mod days;
